<?php

namespace Drupal\sms_netcore\Plugin\SmsGateway;

use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResultStatus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Entity\SmsMessageInterface as SmsMessageEntityInterface;

/**
 * @SmsGateway(
 *   id = "netcore",
 *   label = @Translation("Netcore"),
 *   incoming = FALSE,
 *   outgoing_message_max_recipients = 1,
 * )
 */
class Netcore extends SmsGatewayPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = [];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['netcore'] = [
      '#type' => 'details',
      '#title' => $this->t('Netcore'),
      '#open' => TRUE,
    ];

    $form['netcore']['feedid'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Feed ID'),
      '#default_value' => isset($config['feedid']) ? $config['feedid'] : '' ,
    ];

    $form['netcore']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Name (Optional)'),
      '#default_value' => isset($config['username']) ? $config['username'] : '',
    ];

    $form['netcore']['pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => isset($config['pass']) ? $config['pass'] : '',
    ];

    $form['netcore']['senderid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender ID (Optional)'),
      '#default_value' => isset($config['senderid']) ? $config['senderid'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['feedid'] = trim($form_state->getValue('feedid'));
    $this->configuration['username'] = trim($form_state->getValue('username'));
    $this->configuration['pass'] = trim($form_state->getValue('pass'));
    $this->configuration['senderid'] = trim($form_state->getValue('senderid'));
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {
    $result = new SmsMessageResult();
    
    return $result;
  }

}
